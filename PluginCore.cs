﻿using AcClient;

using Decal.Adapter;

using System;
using System.Runtime.InteropServices;

using static WindowsCursors.Constants;

namespace WindowsCursors
{
    [FriendlyName("WindowsCursors")]
    public unsafe class PluginCore : PluginBase
    {
        private bool pluginStarted = false;

        internal delegate void Del_MouseCursorUpdate(UIElementManager_SetCursor_Args x);
        internal static event Del_MouseCursorUpdate MouseCursorUpdate;

        [DllImport("user32.dll")]
        internal static extern IntPtr LoadCursor(IntPtr hInstance, int lpCursorName);

        [DllImport("user32.dll")]
        internal static extern IntPtr SetCursor(IntPtr handle);

        internal static void Device_SetCursorFromIcon(uint hCursor) => ((delegate* unmanaged[Thiscall]<uint, uint>)(int)Entrypoint.Device__SetCursorFromIcon)(hCursor);

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
        internal delegate void SetCursorDelegate(UIElementManager* _this, uint _cursorDID, uint _xOffset, uint _yOffset, bool _default);

        internal static Hook detour_ClientUISystem_UpdateCursorState = new Hook((int)Entrypoint.UIElementManager__SetCursor, (int)Call.UIElementManager__SetCursor_0x005654D8);
        internal static Hook detour_UIElementManager_CheckCursor = new Hook((int)Entrypoint.UIElementManager__SetCursor, (int)Call.UIElementManager__SetCursor_0x0045AD54);
        internal static Hook detour_UIElementManager_CheckCursor2 = new Hook((int)Entrypoint.UIElementManager__SetCursor, (int)Call.UIElementManager__SetCursor_0x0045AD74);
        internal static Hook detour_UIElementManager_RefreshEvent = new Hook((int)Entrypoint.UIElementManager__SetCursor, (int)Call.UIElementManager__SetCursor_0x0045C692);
        internal static Hook detour_gmUIFlow_gmUIFlow = new Hook((int)Entrypoint.UIElementManager__SetCursor, (int)Call.UIElementManager__SetCursor_0x0047AAD4);

        protected override void Startup()
        {
            var myFunc = new SetCursorDelegate(UIElementManager_SetCursor);
            detour_ClientUISystem_UpdateCursorState.Setup(myFunc);
            detour_UIElementManager_CheckCursor.Setup(myFunc);
            detour_UIElementManager_CheckCursor2.Setup(myFunc);
            detour_UIElementManager_RefreshEvent.Setup(myFunc);
            detour_gmUIFlow_gmUIFlow.Setup(myFunc);
            Device_SetCursorFromIcon(0); // disable the cursor that ac had already set
            MouseCursorUpdate += SetWindowsCursor;
        }
        protected override void Shutdown()
        {
        }
        internal static void UIElementManager_SetCursor(UIElementManager* _this, uint _cursorDID, uint _xOffset, uint _yOffset, bool _default)
        {
            UIElementManager_SetCursor_Args x = new UIElementManager_SetCursor_Args()
            {
                _this = _this,
                _cursorDID = (CursorDID)_cursorDID,
                _xOffset = _xOffset,
                _yOffset = _yOffset,
                _default = _default
            };

            if (x._default)
            {
                x._this->m_defaultCursorDID = (uint)x._cursorDID;
                x._this->m_defaultCursorHotX = (int)x._xOffset;
                x._this->m_defaultCursorHotY = (int)x._yOffset;
            }

            if (x._cursorDID != 0
              && ((uint)x._cursorDID != x._this->m_lastCursorDID
               || x._xOffset != x._this->m_lastCursorHotX
               || x._yOffset != x._this->m_lastCursorHotY))
            {
                x._this->m_lastCursorDID = (uint)x._cursorDID;
                x._this->m_lastCursorHotX = (int)x._xOffset;
                x._this->m_lastCursorHotY = (int)x._yOffset;

                MouseCursorUpdate?.Invoke(x);
            }
        }
        private void SetWindowsCursor(UIElementManager_SetCursor_Args x)
        {
            WinCurs cur = WinCurs.IDC_ARROW;

            #region sw
            switch (x._cursorDID)
            {
                case CursorDID.Normal:
                    break;
                case CursorDID.NormalHot:
                    break;
                case CursorDID.NormalRed:
                    break;
                case CursorDID.NormalHotRed:
                    break;
                case CursorDID.NormalBlue:
                    break;
                case CursorDID.NormalHotBlue:
                    break;
                case CursorDID.Scrollup:
                    break;
                case CursorDID.Scrollnone:
                    break;
                case CursorDID.Scrolldown:
                    break;
                case CursorDID.Look:
                    cur = WinCurs.IDC_HELP;
                    break;
                case CursorDID.Usehand:
                    cur = WinCurs.IDC_HAND;
                    break;
                case CursorDID.Usetarget:
                    cur = WinCurs.IDC_CROSS;
                    break;
                case CursorDID.Busy:
                    cur = WinCurs.IDC_APPSTARTING;
                    break;
                case CursorDID.BusyHot:
                    cur = WinCurs.IDC_APPSTARTING;
                    break;
                case CursorDID.ArrowBentLeftLanky:
                    break;
                case CursorDID.ArrowBentLeftSkinny:
                    break;
                case CursorDID.ArrowBentLeftFat:
                    break;
                case CursorDID.ArrowUpLong:
                    break;
                case CursorDID.ArrowUpMed:
                    break;
                case CursorDID.ArrowUpShort:
                    break;
                case CursorDID.ArrowBentRightLanky:
                    break;
                case CursorDID.ArrowBentRightSkinny:
                    break;
                case CursorDID.ArrowBentRightFat:
                    break;
                case CursorDID.ArrowCurlyLeft:
                    break;
                case CursorDID.Move:
                    cur = WinCurs.IDC_SIZEALL;
                    break;
                case CursorDID.ArrowCurlyRight:
                    break;
                case CursorDID.ArrowLeftLong:
                    break;
                case CursorDID.ArrowLeftMed:
                    break;
                case CursorDID.ArrowLeftShort:
                    break;
                case CursorDID.ArrowDownShort:
                    break;
                case CursorDID.ArrowDownMed:
                    break;
                case CursorDID.ArrowDownLong:
                    break;
                case CursorDID.ArrowRightLong:
                    break;
                case CursorDID.ArrowRightMed:
                    break;
                case CursorDID.ArrowRightShort:
                    break;
                case CursorDID.ArrowsVertical:
                    cur = WinCurs.IDC_SIZENS;
                    break;
                case CursorDID.TargetDisabled:
                    cur = WinCurs.IDC_NO;
                    break;
                case CursorDID.Bullseye:
                    cur = WinCurs.IDC_PIN;
                    break;
                case CursorDID.ArrowsAll:
                    cur = WinCurs.IDC_SIZEALL;
                    break;
                case CursorDID.ArrowsTLBR:
                    cur = WinCurs.IDC_SIZENWSE;
                    break;
                case CursorDID.ArrowsBLTR:
                    cur = WinCurs.IDC_SIZENESW;
                    break;
                case CursorDID.ArrowsHorizontal:
                    cur = WinCurs.IDC_SIZEWE;
                    break;
                default:
                    break;
            }
            #endregion

            IntPtr hCursor = LoadCursor((IntPtr)0, (int)cur);
            SetCursor(hCursor);
        }
    }
}
