﻿using AcClient;

using System.Runtime.InteropServices;

using static WindowsCursors.Constants;

namespace WindowsCursors
{
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct UIElementManager_SetCursor_Args
    {
        public UIElementManager* _this;
        public CursorDID _cursorDID;
        public uint _xOffset;
        public uint _yOffset;
        public bool _default;

        public void CallAnterior()
        {
            _this->SetCursor((uint)_cursorDID, (int)_xOffset, (int)_yOffset, (byte)((_default) ? 0x1 : 0x0));
        }

        public override string ToString()
        {
            return $"c: {_cursorDID:X} x: {_xOffset} y: {_yOffset} d: {_default}";
        }
    }
}
