﻿namespace WindowsCursors
{
    internal class Constants
    {
        public enum WinCurs
        {
            IDC_ARROW = 32512,
            IDC_IBEAM = 32513,
            IDC_WAIT = 32514,
            IDC_CROSS = 32515,
            IDC_UPARROW = 32516,
            /// <summary>
            /// OBSOLETE: use IDC_SIZEALL
            /// </summary>
            IDC_SIZE = 32640,
            /// <summary>
            /// OBSOLETE: use IDC_ARROW
            /// </summary>
            IDC_ICON = 32641,
            IDC_SIZENWSE = 32642,
            IDC_SIZENESW = 32643,
            IDC_SIZEWE = 32644,
            IDC_SIZENS = 32645,
            IDC_SIZEALL = 32646,
            IDC_NO = 32648,
            IDC_HAND = 32649,
            IDC_APPSTARTING = 32650,
            IDC_HELP = 32651,
            IDC_PIN = 32671,
            IDC_PERSON = 32672,
        }
        public enum CursorDID : uint
        {
            Normal = 0x06004d68,
            NormalHot = 0x06004d69,
            NormalRed = 0x06004d6a,
            NormalHotRed = 0x06004d6b,
            NormalBlue = 0x06004d6c,
            NormalHotBlue = 0x06004d6d,
            Scrollup = 0x06004d6e,
            Scrollnone = 0x06004d6f,
            Scrolldown = 0x06004d70,
            Look = 0x06004d71,
            Usehand = 0x06004d72,
            Usetarget = 0x06004d73,
            Busy = 0x06004d74,
            BusyHot = 0x06004d75,
            ArrowBentLeftLanky = 0x06004d76,
            ArrowBentLeftSkinny = 0x06004d77,
            ArrowBentLeftFat = 0x06004d78,
            ArrowUpLong = 0x06004d79,
            ArrowUpMed = 0x06004d7a,
            ArrowUpShort = 0x06004d7b,
            ArrowBentRightLanky = 0x06004d7c,
            ArrowBentRightSkinny = 0x06004d7d,
            ArrowBentRightFat = 0x06004d7e,
            ArrowCurlyLeft = 0x06004d7f,
            Move = 0x06004d80,
            ArrowCurlyRight = 0x06004d81,
            ArrowLeftLong = 0x06004d82,
            ArrowLeftMed = 0x06004d83,
            ArrowLeftShort = 0x06004d84,
            ArrowDownShort = 0x06004d85,
            ArrowDownMed = 0x06004d86,
            ArrowDownLong = 0x06004d87,
            ArrowRightLong = 0x06004d88,
            ArrowRightMed = 0x06004d89,
            ArrowRightShort = 0x06004d8a,
            ArrowsVertical = 0x06005e66,
            TargetDisabled = 0x06005e6a,
            Bullseye = 0x06005e6b,
            ArrowsAll = 0x06006119,
            ArrowsTLBR = 0x06006126,
            ArrowsBLTR = 0x06006127,
            ArrowsHorizontal = 0x06006128,
        }
    }
}
