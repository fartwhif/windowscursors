# WindowsCursors

Replaces the Asheron's Call mouse cursors.  Have your current windows cursors and cursor settings in-game.

## Requirements

- Decal 2.9.8.3 or better
- .NET Framework 4.8.1 or better

## Getting started

Run the installer.  Open the Decal agent and enable the plugin.